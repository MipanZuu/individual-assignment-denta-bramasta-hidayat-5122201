package com.s3.servicewebsitebackend.controller;

import com.s3.servicewebsitebackend.business.UsersService;
import com.s3.servicewebsitebackend.domain.UserUpdateRequest;
import com.s3.servicewebsitebackend.persistence.entity.UsersEntity;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UsersController {
    private final UsersService usersService;

    @PostMapping()
    public ResponseEntity<UsersEntity> createUsers(@RequestBody @Valid UsersEntity request){
        UsersEntity response = usersService.createUsers(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping("{user_id}")
    public ResponseEntity<UsersEntity> getUserID(@PathVariable("user_id") int user_id) {
        final Optional<UsersEntity> userID = usersService.getUserID(user_id);
        return userID.map(usersEntity -> ResponseEntity.ok().body(usersEntity)).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<UsersEntity>> getAllUsers() {
        return ResponseEntity.ok(usersService.getAllUsers());
    }

    @DeleteMapping("{user_id}")
    public ResponseEntity<Void> deleteUser(@PathVariable int user_id) {
        usersService.deleteUser(user_id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{user_id}")
    public ResponseEntity<Void> updateUser(@PathVariable("user_id") int user_id, @RequestBody @Valid UserUpdateRequest request) {
        request.setUser_id(user_id);
        usersService.updateUser(request);
        return ResponseEntity.noContent().build();
    }
}
