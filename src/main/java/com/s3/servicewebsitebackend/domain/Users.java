package com.s3.servicewebsitebackend.domain;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Entity
@Table(name = "Users", uniqueConstraints = @UniqueConstraint(columnNames = "user_email"))
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", length = 7)
    private int user_id;
    
    @Column(name = "username", length = 255)
    private String username;

    @Column(name = "firstname", length = 255)
    private String firstname;

    @Column(name = "lastname", length = 255)
    private String lastname;

    @Column(name = "user_email", length = 255)
    private String user_email;

    @Column(name = "user_password", length = 50)
    private String user_password;

    @Column(name = "user_role", length = 20)
    private String user_role;
}
