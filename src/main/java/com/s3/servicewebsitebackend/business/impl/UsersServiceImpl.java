package com.s3.servicewebsitebackend.business.impl;

import com.s3.servicewebsitebackend.business.UsersService;
import com.s3.servicewebsitebackend.business.exception.InvalidUsersException;
import com.s3.servicewebsitebackend.domain.UserUpdateRequest;
import com.s3.servicewebsitebackend.persistence.UsersRepository;
import com.s3.servicewebsitebackend.persistence.entity.UsersEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UsersServiceImpl implements UsersService {
    private final UsersRepository usersRepository;

    @Override
    public UsersEntity createUsers(UsersEntity user){
        return usersRepository.save(user);
    }

    @Override
    public Optional<UsersEntity> getUserID(int user_id) {
        return usersRepository.findById(user_id);
    }

    @Override
    public List<UsersEntity> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(int user_id) {
        this.usersRepository.deleteById(user_id);
    }

    @Override
    public void updateUser(UserUpdateRequest request) {
        Optional<UsersEntity> userOptional = usersRepository.findById(request.getUser_id());
        if (userOptional.isEmpty()) {
            throw new InvalidUsersException("INVALID-USER_ID");
        }
        UsersEntity user = userOptional.get();
        updateFieldUser(request, user);
    }
    private void updateFieldUser(UserUpdateRequest request, UsersEntity user) {
        user.setUsername(request.getUsername());
        user.setFirstname(request.getFirstname());
        user.setLastname(request.getLastname());
        user.setUser_email(request.getUser_email());
        user.setUser_password(request.getUser_password());
        user.setUser_role(request.getUser_role()); //NEED TO BE CHANGE !!!!
        usersRepository.save(user);
    }

}
