package com.s3.servicewebsitebackend.business.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidUsersException extends ResponseStatusException {
    public InvalidUsersException(String errorCode) {
        super(HttpStatus.BAD_REQUEST, errorCode);
    }
}
