package com.s3.servicewebsitebackend.business;
import com.s3.servicewebsitebackend.domain.UserUpdateRequest;
import com.s3.servicewebsitebackend.persistence.entity.UsersEntity;

import java.util.List;
import java.util.Optional;

public interface UsersService {
    UsersEntity createUsers(UsersEntity request);
    Optional<UsersEntity> getUserID(int user_id);
    List<UsersEntity> getAllUsers();
    void deleteUser(int user_id);
    void updateUser(UserUpdateRequest request);
}
